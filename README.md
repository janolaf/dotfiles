# dotfiles

My corrent config for Neovim and Zsh.

<pre>
├── README.md
├── nvim
│   ├── configs
│   │   ├── commands.vim
│   │   ├── general.vim
│   │   ├── plugin-settings.vim
│   │   ├── plugins.vim
│   │   └── ui.vim
│   ├── ftplugin
│   │   ├── go.vim
│   │   ├── javascript.vim
│   │   └── python.vim
│   └── init.vim
└── zsh
    ├── print256colors.sh
    ├── zsh
    │   ├── alias.zsh
    │   ├── functions.zsh
    │   ├── go.zsh
    │   ├── linux.zsh
    │   ├── macos.zsh
    │   ├── powerlevel9k.zsh
    │   └── python.zsh
    ├── zshenv
    └── zshrc
</pre>
