#!/usr/bin/env zsh
# Macos specific zsh configuration

# Python path. needed for user install pip on macos
export PATH=~/Library/Python/3.7/bin:$PATH

# Alias specifc to macos
alias grep='grep --color=auto --exclude-dir={.bzr,CVS,.git,.hg,.svn}'

if type 'exa' &> /dev/null; then
    alias ls='/usr/local/bin/exa'
elif type 'gls' &> /dev/null; then
    alias ls='/usr/local/bin/gls --color=always -p'
else
    setenv LSCOLORS ExGxdxdxCxDxDxBxBxegeg
    alias ls='ls -FG'
fi

if type 'nvim' &> /dev/null; then
    alias vi='/usr/local/bin/nvim'
    export EDITOR='nvim'
fi

alias whatIP='ifconfig | grep inet'
