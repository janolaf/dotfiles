# go.zsh
#export PATH=$PATH:$GOPATH/bin

case $OSTYPE in
    darwin*)
        export GOROOT=/usr/local/opt/go/libexec
        export PATH=$PATH:$GOROOT/bin
esac
