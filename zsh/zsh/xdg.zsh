# this file is to for XDG conformity

# MySQL
export MYSQL_HISTFILE="$XDG_DATA_HOME"/mysql_history

# PostgreSQL
export PSQLRC="$XDG_CONFIG_HOME/pg/psqlrc"
export PSQL_HISTORY="$XDG_CACHE_HOME/pg/psql_history"
export PGPASSFILE="$XDG_CONFIG_HOME/pg/pgpass"
export PGSERVICEFILE="$XDG_CONFIG_HOME/pg/pg_service.conf"

# export Deno home

# check if PostgreSQL is installed. if PostgreSQL is install, check if direcotry 
# pg  exists and create the direcotry if not
if type 'psql' %> /dev/null; then
    if [ ! -d "$XDG_CONFIG_HOME/pg"]; then 
	echo "creating directories for PostgreSQL"
	mkdir "$XDG_CONFIG_HOME"/pg
	mkdir "$XDG_CACHE_HOME"/pg
    fi
fi
# npm config and node.js
export NODE_REPL_HISTORY="$XDG_DATA_HOME"/node_repl_history
export NPM_CONFIG_USERCONFIG=$XDG_CONFIG_HOME/npm/npmrc

# Go Stuff
#export GOPATH="$XDG_DATA_HOME"/go 
#export GOBIN="$XDG_DATA_HOME"/go/bin

# Vagrant
export VAGRANT_HOME="$XDG_DATA_HOME"/vagrant
export VAGRANT_ALIAS_FILE="$XDG_DATA_HOME"/vagrant/aliases

# GTK1 and GTK2
export GTK_RC_FILES="$XDG_CONFIG_HOME"/gtk-1.0/gtkrc
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc

# Python stuff
export IPYTHONDIR="$XDG_CONFIG_HOME"/jupyter
export JUPYTER_CONFIG_DIR="$XDG_CONFIG_HOME"/jupyter

# most
export MOST_INITFILE="$XDG_CONFIG_HOME"/mostrc

# Rust
export CARGO_HOME="$XDG_DATA_HOME"/cargo

# fzf
mkdir -p ~/.config/fzf

export DOTBARE_DIR="$HOME/.config/dotbare"
