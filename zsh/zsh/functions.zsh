# specific functions used

# Function to check if binary exists; if not then install

function macCheckInstall() {
    for var in "$@"; do
        if ! type $var &> /dev/null; then
            echo "Installing $var"
            brew install $var
        fi
    done
}

function dnfCheckInstall() {
    for var in "$@"; do
        if ! type $var &> /dev/null; then
            echo "Installing $var"
            sudo dnf install $var
        fi
    done
}
