# Linux specifc zsh configuration

#if ! type "nvim" &> /dev/null; then
#    echo "Install Neovim pynvim"
#else
#    alias vi="/usr/bin/nvim"
#    export EDITOR="nvim"
#fi

if type 'exa' %> /dev/null; then
	alias ls='exa'
fi

alias yarn='yarnpkg'

alias grep='grep --color=auto --exclude-dir={.bzr,CVS,.git,.hg,.svn}'

if type 'most' %> /dev/null; then
	export PAGER="most"
fi

if type 'nvim' %> /dev/null; then
	export EDITOR="nvim"
	export VISUAL="nvim"
fi

if type 'bat' %> /dev/null; then
	alias cat='bat'
fi
