#!/bin/bash

# Create symbolic links for bash

# Create symbolic links for zsh config
ln -s $HOME/git/dotfiles/zsh/zshrc $HOME/.zshrc
ln -s $HOME/git/dotfiles/zsh/zshenv $HOME/.zshenv
ln -s $HOME/git/dotfiles/zsh/zsh $HOME/.zsh

# Create symbolic links for neovim config
ln -s $HOME/git/dotfiles/nvim/ $HOME/.config/nvim

