" neovim init.vim
if !exists('g:vscode')
    let config = [
        \ "general",
        \ "commands",
        \ "plugins",
        \]
endif

let uname = system('uname -a')

if has('nvim') || exists('g:vscode')
    for file in config
        let x = expand("~/.config/nvim/settings/".file.".vim")
        if filereadable(x)
            execute 'source' x
        endif
    endfor
elseif has('vim')
    for file in config
        let x =expand("~/.vim/".file.".vim")
        if filereadable(x)
            execute 'source' x
        endif
    endfor
endif
