" go.vim config

" don't give |ins-completion-menu| messages.
set shortmess+=c
" always show signcolumns
set signcolumn=yes

"------------------------------------------------------------------------------
" ALE Config for Go
"------------------------------------------------------------------------------

" let b:ale_fixers = {'go': ['goimports', 'gofmt']}

"------------------------------------------------------------------------------
" TagBar config for go
"------------------------------------------------------------------------------

let g:tagbar_type_go = {
      \ 'ctagstype' : 'go',
      \ 'kinds'     : [
          \ 'p:package',
          \ 'i:imports:1',
          \ 'c:constants',
          \ 'v:variables',
          \ 't:types',
          \ 'n:interfaces',
          \ 'w:fields',
          \ 'e:embedded',
          \ 'm:methods',
          \ 'r:constructor',
          \ 'f:functions'
      \ ],
      \ 'sro' : '.',
      \ 'kind2scope' : {
          \ 't' : 'ctype',
          \ 'n' : 'ntype'
      \ },
      \ 'scope2kind' : {
          \ 'ctype' : 't',
          \ 'ntype' : 'n'
      \ },
      \ 'ctagsbin'  : 'gotags',
      \ 'ctagsargs' : '-sort -silent'
      \ }


au Filetype go set noexpandtab
au Filetype go set shiftwidth=4
au Filetype go set softtabstop=4
au Filetype go set tabstop=4

" add colors
let g:go_highlight_build_constraints = 1
let g:go_highlight_extra_types = 1
let g:go_highlight_fields = 1
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_operators = 1
let g:go_highlight_structs = 1
let g:go_highlight_types = 1
let g:go_auto_sameids = 1               " highlight other uses of a variable with cursor

" disable vim-go :GoDef short cut (gd)
" this is handled by LanguageClient [LC]
let g:go_def_mapping_enabled = 0

let g:go_fmt_fail_silently = 1          " needed to play nicely with ALE
let g:go_fmt_command = 'goimports'          " adds autoimport like in python
let g:go_auto_type_info = 1             " cursor highlight variable to see what type it is
