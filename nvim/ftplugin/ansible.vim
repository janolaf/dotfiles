let b:ale_fixers = [ 'ansible-lint' ]

set tabstop=2
set shiftwidth=2
set softtabstop=2
