"-----------------------------------------------------------------------------
" Auto-Install of vim-plug
"-----------------------------------------------------------------------------
if has("nvim")
    if empty(glob('~/.local/share/nvim/site/autoload/plug.vim')) "prefered neovim location
        silent !curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs
                    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
        autocmd VimEnter * PlugInstall
    endif
elseif has("vim")
    if empty(glob('~/.vim/autoload/plug.vim')) "normal vim install location
        silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
                    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
        autocmd VimEnter * PlugInstall | source $MYVIMRC
    endif
endif

"-----------------------------------------------------------------------------
" Start of vim-plug Script
"-----------------------------------------------------------------------------

" to get the same config to work with macvim, vim, and neovim
if has("nvim")
    " install vim-plug in ~/.local/share/nvim/site/autoload/
    call plug#begin(stdpath('data') . '/plugged')
else
    call plug#begin('~/.local/share/vim/plugged')
endif

if has('nvim-0.5')
    Plug 'nvim-treesitter/nvim-treesitter'
"    Plug 'nvim-lua/completion-nvim'
"    Plug 'glepnir/zephyr-nvim'
    Plug 'sainnhe/edge'
else
    Plug 'sheerun/vim-polyglot'			"helps with code highlighting
    Plug 'numirias/semshi', { 'do': ':UpdateRemotePlugins' } "semantic highlighting for python
    " color sceme
    Plug 'joshdick/onedark.vim'
endif
" snippet support
" vsnip supports vscode style snippets
Plug 'hrsh7th/vim-vsnip'
Plug 'hrsh7th/vim-vsnip-integ'			" supports LSP-clients and completion engines

" git plugins
Plug 'tpope/vim-fugitive'
" Plugins used by other plugins
Plug 'Shougo/vimproc.vim', {'do' : 'make'}
Plug 'Shougo/denite.nvim'

" autocomplete AND lint engine
Plug 'neoclide/coc.nvim', { 'branch': 'release' }
" Plug 'dense-analysis/ale'

" general plugins
Plug 'junegunn/fzf.vim'				"fuzzy search integration

Plug 'tpope/vim-surround'
Plug 'thaerkh/vim-indentguides'
Plug 'RRethy/vim-illuminate'
" Plug 'kyazdani42/nvim-web-devicons'			"adds support for devicons in vim
Plug 'editorconfig/editorconfig-vim'		"cross-editor code formatting settings
Plug 'ryanoasis/vim-devicons'

" general programing plugins
Plug 'janko/vim-test'
"Plug 'puremourning/vimspector'			" debuging tool

"airline
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" ----------------------------------------------------------------------------
" Language Plugins
" ----------------------------------------------------------------------------
" Ansible
Plug 'pearofducks/ansible-vim'

" pandoc / markdown
Plug 'vim-pandoc/vim-pandoc', { 'for': [ 'pandoc', 'markdown' ] }
Plug 'vim-pandoc/vim-pandoc-syntax', { 'for': [ 'pandoc', 'markdown' ] }

" csv
Plug 'chrisbra/csv.vim', { 'for': 'csv' }

" latex
Plug 'lervag/vimtex' 

" go-lang
Plug 'fatih/vim-go', { 'do': ':GoInstallBinaries' }
" ----------------------------------------------------------------------------
call plug#end()
" ----------------------------------------------------------------------------
" support for true color
if (empty($TMUX))
    if (has("termguicolors"))
        set termguicolors
    endif
endif

" ------------------------------------------------------------------
colorscheme
" ------------------------------------------------------------------
set background=dark

"g:onedark_terminal_italics = 1
if has('nvim-0.5')
    colorscheme edge
    let g:airline_theme='deus'
else
    colorscheme onedark
endif

" ----------------------------------------------------------------------------
" airline settings
" ----------------------------------------------------------------------------
set laststatus=2 " shows airline
let g:airline#extensions#whitespace#mixed_indent_algo = 0 "because I hate trailings
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ''
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline#extensions#tabline#formatter = 'unique_tail'
let g:airline#extensions#syntastic#enabled = 0   " disable syntastic support

" enable support for ALE
let g:airline#extensions#ale#enabled = 1
let g:airline#extensions#ale#error_symbol = '⨉'
let g:airline#extensions#ale#warning_symbol = '⚠'

let g:airline#extensions#ale#error_symbol = 1
let g:airline#extensions#warning_symbol = 1

" airline branch settings
" enable fugitive support
let g:airline#extensions#branch#enable = 1
" when no branch detected
let g:airline#extensions#branch#empty_message = ''
" limits long branch name to 10 chars
let g:airline#extensions#branch#displayed_head_limit = 10

let g:airline_powerline_fonts = 1

let g:airline_detect_paste = 1
let g:airline_detect_modified = 1
let g:airline_detect_spell = 1

let g:airline_left_sep = ''
let g:airline_right_sep = ''

let g:airline_filetype_overrides = {
    \ 'help': [ 'Help', '%f' ],
    \ 'vim-plug': [ 'Plugins', '' ],
    \ 'vimfiler': [ 'vimfiler', '%{vimfiler#get_status_string()}' ]
    \ }

" exclude airline status line from preview window
let g:airline_exclude_preview = 0

" if fileformat is utf-8[unix], don;t display on statusline
let g:airline#parts#ffenc#skip_expected_string='utf-8[unix]'

if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif

"alters percentage, line number and column number
" let g:airline_section_z =


" support for coc.nvim in airline
let g:airline#extensions#coc#enable = 1
let g:airline_section_error = '%{airline#util#wrap(airline#extensions#coc#get_error(),0)}'
let g:airline_section_warning = '%{airline#util#wrap(airline#extensions#coc#get_warning(),0)}'


" ----------------------------------------------------------------------------
" vim-indentguides
" ----------------------------------------------------------------------------

let g:indentguides_ignorelist = ['text', 'help']

let g:indentguides_spacechar = '┊'
let g:indentguides_tapchar = '┊'

" ----------------------------------------------------------------------------
" bufferline settings
" ----------------------------------------------------------------------------
let g:bufferline_echo = 0 " keep while using airline

" ----------------------------------------------------------------------------
" vim-illuminate
" ----------------------------------------------------------------------------
let g:Illuminate_delay = 250  " time delay of highlight
" let g:Illuminate_ftblacklist = ['nerdtree']     " disabled for filtype

" ----------------------------------------------------------------------------
" vim-go
" ----------------------------------------------------------------------------
" vim-go settings are in ftplugin/go.vim

" ----------------------------------------------------------------------------
" Better Whitespaces
" ----------------------------------------------------------------------------
let g:better_whitespace_enabled = 1
let g:strip_whitespace_on_save = 1

" stripes whitespace on save
" autocmd BufWritePre * StripWhitespace

" ----------------------------------------------------------------------------
" coc.nvim
" ----------------------------------------------------------------------------
" vim language server
let g:markdown_fence_languages = [ 'vim', 'help' ]

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current
" position. Coc only does snippet and additional edit on confirm.
if has('patch8.1.1068')
  " Use `complete_info` if your (Neo)Vim version supports it.
  inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"
else
  imap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
endif

" Use `[g` and `]g` to navigate diagnostics
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window.
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

" Formatting selected code.
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder.
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap keys for applying codeAction to the current line.
nmap <leader>ac  <Plug>(coc-codeaction)
" Apply AutoFix to problem on the current line.
nmap <leader>qf  <Plug>(coc-fix-current)

" Introduce function text object
" NOTE: Requires 'textDocument.documentSymbol' support from the language server.
xmap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap if <Plug>(coc-funcobj-i)
omap af <Plug>(coc-funcobj-a)

" Use <TAB> for selections ranges.
" NOTE: Requires 'textDocument/selectionRange' support from the language server.
" coc-tsserver, coc-python are the examples of servers that support it.
nmap <silent> <TAB> <Plug>(coc-range-select)
xmap <silent> <TAB> <Plug>(coc-range-select)

" Add `:Format` command to format current buffer.
command! -nargs=0 Format :call CocAction('format')

" Add `:Fold` command to fold current buffer.
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" Add `:OR` command for organize imports of the current buffer.
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" Add (Neo)Vim's native statusline support.
" NOTE: Please see `:h coc-status` for integrations with external plugins that
" provide custom statusline: lightline.vim, vim-airline.
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

" Mappings using CoCList:
" Show all diagnostics.
nnoremap <silent> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions.
nnoremap <silent> <space>e  :<C-u>CocList extensions<cr>
" Show commands.
nnoremap <silent> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document.
nnoremap <silent> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols.
nnoremap <silent> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list.
nnoremap <silent> <space>p  :<C-u>CocListResume<CR>

" coc extensions
let g:coc_global_extensions = [
	    \ 'coc-json',
            \ 'coc-yank',
            \ 'coc-python',
            \ 'coc-lists',
            \ 'coc-snippets',
            \ 'coc-vimlsp',
	    \ 'coc-go',
	    \ 'coc-sh',
	    \ 'coc-java',
	    \ 'coc-highlight',
	    \ 'coc-xml',
	    \ 'coc-yaml',
	    \ 'coc-vimtex',
	    \ 'coc-clangd'
	    \ ]

" snippet support in coc.nvim

" Use <C-l> for trigger snippet expand.
imap <C-l> <Plug>(coc-snippets-expand)

" Use <C-j> for select text for visual placeholder of snippet.
vmap <C-j> <Plug>(coc-snippets-select)

" Use <C-j> for jump to next placeholder, it's default of coc.nvim
let g:coc_snippet_next = '<c-j>'

" Use <C-k> for jump to previous placeholder, it's default of coc.nvim
let g:coc_snippet_prev = '<c-k>'

" Use <C-j> for both expand and jump (make expand higher priority.)
imap <C-j> <Plug>(coc-snippets-expand-jump)

" tab to trigger completion 

inoremap <silent><expr> <TAB>
      \ pumvisible() ? coc#_select_confirm() :
      \ coc#expandableOrJumpable() ? "\<C-r>=coc#rpc#request('doKeymap', ['snippets-expand-jump',''])\<CR>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

let g:coc_snippet_next = '<tab>'

" support for go-vim snippets
"snippets.ultisnips.directories: [
"    "UltiSnips",
"    "gosnippets/UltiSnips"
"    ],


" coc.nvim go settings are in ftplugin/go.vim

" ------------------------------------------------------------------
"  ALE
" ------------------------------------------------------------------
" let deoplete or coc do completion
let g:ale_lint_on_enter = 0
let g:ale_completion_enabled = 0
let g:ale_sign_error = '⨉'
let g:ale_sign_warning = '⚠'
let g:ale_sign_info = 'ℹ'
let g:ale_statusline_format = ['⨉ %d', '⚠ %d', '⬥ ok']
let g:ale_fix_on_save = 1   "fix files on save
let g:ale_open_list = 1 " opens errors like synaptic
let g:ale_javascript_prettier_use_local_config = 1
let g:ale_javascript_eslint_use_local_config = 1
let g:ALESymbolSearch = 1
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'

let g:ale_fixers = {
            \   '*': ['remove_trailing_lines', 'trim_whitespace'],
            \ }

" ------------------------------------------------------------------
" fzf
" ------------------------------------------------------------------
" use floating windows for fzf
if has('nvim_open_win')
	let g:fzf_layout = { 'window': 'call OpenFloatingWin()' }

	function OpenFlaotingWin()
		let height = &lines - 3
		let width = float2nr(&columns - (&columns * 2 / 10))
		let col = float2nr((&colums - width) / 2)

		let opts = {
			\ 'relative': 'editor',
			\ 'row': height * 0.3,
			\ 'col': col + 30,
			\ 'width': width * 2 / 3,
			\ 'height': height / 2
			\ }

		let buf = nvim_create_buf(v:false, v:true)
		let win = nvim_open_win(buf, v:true, opts)

		call setwinvar(win, '&winhl', 'Normal:Pmenu')

		setlocal
			\ buftype=nofile
			\ nobuflisted
			\ bufhidden=hide
			\ nonumber
			\ norelativenumber
			\ signcolumn=no
	endfunction
endif

" ------------------------------------------------------------------
" Vista
" ------------------------------------------------------------------
" only works with LSP not ctags
let g:vista_icon_indent = ["▸ ", ""]

let g:vista_default_executive = 'coc'

" example of declaring executive different from default
" let g:vista_executive_for = {
"	\ 'python3': 'coc',
"	\ }

let g:vista_fzf_preview = ['right:50%']
let g:vista#renderer#enable_icon = 1

" ------------------------------------------------------------------
" Vimtex
"
" ------------------------------------------------------------------
let g:tex_flavor = 'latex'

