set nocompatible

set mouse=a

" security
set modelines=0
set encoding=utf-8

" fuzzy find
set path+=**
" lazy file name tab completion
set wildmode=longest,list,full
set wildmenu
set wildignorecase
" ignore files vim doesnt use
set wildignore+=.git,.hg,.svn
set wildignore+=*.aux,*.out,*.toc
set wildignore+=*.o,*.obj,*.exe,*.dll,*.manifest,*.rbc,*.class
set wildignore+=*.ai,*.bmp,*.gif,*.ico,*.jpg,*.jpeg,*.png,*.psd,*.webp
set wildignore+=*.avi,*.divx,*.mp4,*.webm,*.mov,*.m2ts,*.mkv,*.vob,*.mpg,*.mpeg
set wildignore+=*.mp3,*.oga,*.ogg,*.wav,*.flac
set wildignore+=*.eot,*.otf,*.ttf,*.woff
set wildignore+=*.doc,*.pdf,*.cbr,*.cbz
set wildignore+=*.zip,*.tar.gz,*.tar.bz2,*.rar,*.tar.xz,*.kgb
set wildignore+=*.swp,.lock,.DS_Store,._*

" case insensitive search
set ignorecase
set smartcase
set infercase

" searching
set hlsearch
set incsearch 				" highlight while searching with / or ?
if has("nvim")
  set inccommand=split
endif

" indentation and spaces
set shiftwidth=4			" use indents of 4 spaces
" set tabstop=4				" an indentation every four columns
set softtabstop=4			" let backspace delete indent
set autoindent				" enable auto indentation
" set expandtab
set smarttab

" set clipboard=unnamed
" set leader key to comma
"let mapleader=","
" check if running macos. has("mac") may not work
" has("osx") maybe vim only
" system("uname -s") for older versons of vim/neovim
if has("mac") || has("osx") || system("uname -s") == "Darwin\n"
    "let g:python_host_prog = '/usr/local/bin/python2'
    let g:python3_host_prog = '/usr/local/bin/python3'
    " decrease startup time by telling clipboard.vim to stop looking on macos'
    let g:clipboard = {
      \ 'name': 'pbcopy',
      \ 'copy': {
      \     '+': 'pbcopy',
      \     '*': 'pbcopy',
      \     },
      \ 'paste': {
      \     '+': 'pbcopy',
      \     '*': 'pbcopy',
      \     },
      \ 'cache_enabled': 0,
      \ }
endif

if has('linux')
    let g:clipboard = unnamedplus
	set shell=/bin/bash		" set neovim term to bash in linux
endif

" show matching brackets/parenthesis
set showmatch

" hide mode display
"set noshowmode

" syntax highlighting
syntax enable
set synmaxcol=512
filetype plugin on

" stop unnecessary rendering
"set lazyredraw
" so I can move to another buffer and suggested by coc.nvim
set hidden

" no line wrapping
set nowrap

" better display for messages
set cmdheight=2

" highlight cursor
set cursorline

" split style for help windows
autocmd Filetype help wincmd L		    " open help files virtucally rather then horizontally
set splitright                          " vsplit window to the right
set splitbelow                          " vsplit window to tthe bottom

set showcmd                             " in status bar, show incomplete commands as they are typed
if exists(":AirlineToggle")
    set noshowmode                          " so not show current mode (inseet, Visual, Replace) status. status shown in airline
endif

set visualbell                          " show visual when error occurs
set noerrorbells                        " stop error sounds from playing

" tree style file explorer
let g:netrw_liststyle=3
let g:netrw_browse_split=4
let g:netrw_winsize=25

" draw a line to see 80 character limit
let &colorcolumn = join(range(81,999),",")
let &colorcolumn="80,".join(range(400,999),",")

" coc.nvim specific config
" improve performance of diagniostic messsages. Default is 4000
set updatetime=300
set signcolumn=yes							"always show signcolumns
